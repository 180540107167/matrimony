// Generated code from Butter Knife. Do not modify!
package com.example.matrimony.activity;

import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.example.matrimony.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AddCandidateActivity_ViewBinding implements Unbinder {
  private AddCandidateActivity target;

  @UiThread
  public AddCandidateActivity_ViewBinding(AddCandidateActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AddCandidateActivity_ViewBinding(AddCandidateActivity target, View source) {
    this.target = target;

    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.etName = Utils.findRequiredViewAsType(source, R.id.etName, "field 'etName'", EditText.class);
    target.etFatherName = Utils.findRequiredViewAsType(source, R.id.etFatherName, "field 'etFatherName'", EditText.class);
    target.etSurName = Utils.findRequiredViewAsType(source, R.id.etSurName, "field 'etSurName'", EditText.class);
    target.rbMale = Utils.findRequiredViewAsType(source, R.id.rbMale, "field 'rbMale'", RadioButton.class);
    target.rbFemale = Utils.findRequiredViewAsType(source, R.id.rbFemale, "field 'rbFemale'", RadioButton.class);
    target.etDOB = Utils.findRequiredViewAsType(source, R.id.etDOB, "field 'etDOB'", EditText.class);
    target.etEmailAddress = Utils.findRequiredViewAsType(source, R.id.etEmailAddress, "field 'etEmailAddress'", EditText.class);
    target.etPhoneNumber = Utils.findRequiredViewAsType(source, R.id.etPhoneNumber, "field 'etPhoneNumber'", EditText.class);
    target.spCity = Utils.findRequiredViewAsType(source, R.id.spCity, "field 'spCity'", Spinner.class);
    target.spLanguage = Utils.findRequiredViewAsType(source, R.id.spLanguage, "field 'spLanguage'", Spinner.class);
    target.chbActCricket = Utils.findRequiredViewAsType(source, R.id.chbActCricket, "field 'chbActCricket'", CheckBox.class);
    target.chbActFootBall = Utils.findRequiredViewAsType(source, R.id.chbActFootBall, "field 'chbActFootBall'", CheckBox.class);
    target.chbActHockey = Utils.findRequiredViewAsType(source, R.id.chbActHockey, "field 'chbActHockey'", CheckBox.class);
    target.btnSubmit = Utils.findRequiredViewAsType(source, R.id.btnSubmit, "field 'btnSubmit'", Button.class);
    target.etDOBTime = Utils.findRequiredViewAsType(source, R.id.etDOBTime, "field 'etDOBTime'", EditText.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AddCandidateActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.etName = null;
    target.etFatherName = null;
    target.etSurName = null;
    target.rbMale = null;
    target.rbFemale = null;
    target.etDOB = null;
    target.etEmailAddress = null;
    target.etPhoneNumber = null;
    target.spCity = null;
    target.spLanguage = null;
    target.chbActCricket = null;
    target.chbActFootBall = null;
    target.chbActHockey = null;
    target.btnSubmit = null;
    target.etDOBTime = null;
  }
}
