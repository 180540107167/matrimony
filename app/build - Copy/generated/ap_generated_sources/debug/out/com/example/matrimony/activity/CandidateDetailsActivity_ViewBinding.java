// Generated code from Butter Knife. Do not modify!
package com.example.matrimony.activity;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.example.matrimony.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CandidateDetailsActivity_ViewBinding implements Unbinder {
  private CandidateDetailsActivity target;

  @UiThread
  public CandidateDetailsActivity_ViewBinding(CandidateDetailsActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CandidateDetailsActivity_ViewBinding(CandidateDetailsActivity target, View source) {
    this.target = target;

    target.tvFullName = Utils.findRequiredViewAsType(source, R.id.tvFullName, "field 'tvFullName'", TextView.class);
    target.tvEmailAddress = Utils.findRequiredViewAsType(source, R.id.tvEmailAddress, "field 'tvEmailAddress'", TextView.class);
    target.tvPhoneNumber = Utils.findRequiredViewAsType(source, R.id.tvPhoneNumber, "field 'tvPhoneNumber'", TextView.class);
    target.tvCity = Utils.findRequiredViewAsType(source, R.id.tvCity, "field 'tvCity'", TextView.class);
    target.tvLanguage = Utils.findRequiredViewAsType(source, R.id.tvLanguage, "field 'tvLanguage'", TextView.class);
    target.tvHobbies = Utils.findRequiredViewAsType(source, R.id.tvHobbies, "field 'tvHobbies'", TextView.class);
    target.btnUpdate = Utils.findRequiredViewAsType(source, R.id.btnUpdate, "field 'btnUpdate'", Button.class);
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.tvDob = Utils.findRequiredViewAsType(source, R.id.tvDob, "field 'tvDob'", TextView.class);
    target.tvDobTime = Utils.findRequiredViewAsType(source, R.id.tvDobTime, "field 'tvDobTime'", TextView.class);
    target.tvAge = Utils.findRequiredViewAsType(source, R.id.tvAge, "field 'tvAge'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CandidateDetailsActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvFullName = null;
    target.tvEmailAddress = null;
    target.tvPhoneNumber = null;
    target.tvCity = null;
    target.tvLanguage = null;
    target.tvHobbies = null;
    target.btnUpdate = null;
    target.toolbar = null;
    target.tvDob = null;
    target.tvDobTime = null;
    target.tvAge = null;
  }
}
