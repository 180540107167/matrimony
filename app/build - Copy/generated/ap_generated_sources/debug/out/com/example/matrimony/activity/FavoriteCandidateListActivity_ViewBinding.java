// Generated code from Butter Knife. Do not modify!
package com.example.matrimony.activity;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.example.matrimony.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FavoriteCandidateListActivity_ViewBinding implements Unbinder {
  private FavoriteCandidateListActivity target;

  @UiThread
  public FavoriteCandidateListActivity_ViewBinding(FavoriteCandidateListActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public FavoriteCandidateListActivity_ViewBinding(FavoriteCandidateListActivity target,
      View source) {
    this.target = target;

    target.rcvUserList = Utils.findRequiredViewAsType(source, R.id.rcvUserList, "field 'rcvUserList'", RecyclerView.class);
    target.tvNoDataFound = Utils.findRequiredViewAsType(source, R.id.tvNoDataFound, "field 'tvNoDataFound'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    FavoriteCandidateListActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rcvUserList = null;
    target.tvNoDataFound = null;
  }
}
