// Generated code from Butter Knife. Do not modify!
package com.example.matrimony.activity;

import android.view.View;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.cardview.widget.CardView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.example.matrimony.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class HomeActivity_ViewBinding implements Unbinder {
  private HomeActivity target;

  private View view7f080079;

  private View view7f08007b;

  private View view7f08007c;

  private View view7f08007a;

  private View view7f080174;

  @UiThread
  public HomeActivity_ViewBinding(HomeActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public HomeActivity_ViewBinding(final HomeActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.cvActAddCandidate, "field 'cvActAddCandidate' and method 'onCvActAddCandidateClicked'");
    target.cvActAddCandidate = Utils.castView(view, R.id.cvActAddCandidate, "field 'cvActAddCandidate'", CardView.class);
    view7f080079 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onCvActAddCandidateClicked();
      }
    });
    view = Utils.findRequiredView(source, R.id.cvActListCandidate, "field 'cvActListCandidate' and method 'onCvActListCandidateClicked'");
    target.cvActListCandidate = Utils.castView(view, R.id.cvActListCandidate, "field 'cvActListCandidate'", CardView.class);
    view7f08007b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onCvActListCandidateClicked();
      }
    });
    view = Utils.findRequiredView(source, R.id.cvActSearchCandidate, "field 'cvActSearchCandidate' and method 'onCvActSearchCandidateClicked'");
    target.cvActSearchCandidate = Utils.castView(view, R.id.cvActSearchCandidate, "field 'cvActSearchCandidate'", CardView.class);
    view7f08007c = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onCvActSearchCandidateClicked();
      }
    });
    view = Utils.findRequiredView(source, R.id.cvActFavoriteCandidate, "field 'cvActFavoriteCandidate' and method 'onCvActFavoriteCandidateClicked'");
    target.cvActFavoriteCandidate = Utils.castView(view, R.id.cvActFavoriteCandidate, "field 'cvActFavoriteCandidate'", CardView.class);
    view7f08007a = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onCvActFavoriteCandidateClicked();
      }
    });
    view = Utils.findRequiredView(source, R.id.toolbar, "method 'onToolbarClicked'");
    view7f080174 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onToolbarClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    HomeActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.cvActAddCandidate = null;
    target.cvActListCandidate = null;
    target.cvActSearchCandidate = null;
    target.cvActFavoriteCandidate = null;

    view7f080079.setOnClickListener(null);
    view7f080079 = null;
    view7f08007b.setOnClickListener(null);
    view7f08007b = null;
    view7f08007c.setOnClickListener(null);
    view7f08007c = null;
    view7f08007a.setOnClickListener(null);
    view7f08007a = null;
    view7f080174.setOnClickListener(null);
    view7f080174 = null;
  }
}
