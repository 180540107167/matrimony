// Generated code from Butter Knife. Do not modify!
package com.example.matrimony.activity;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.example.matrimony.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SearchCandidateActivity_ViewBinding implements Unbinder {
  private SearchCandidateActivity target;

  @UiThread
  public SearchCandidateActivity_ViewBinding(SearchCandidateActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SearchCandidateActivity_ViewBinding(SearchCandidateActivity target, View source) {
    this.target = target;

    target.etUserSearch = Utils.findRequiredViewAsType(source, R.id.etUserSearch, "field 'etUserSearch'", EditText.class);
    target.rcvUserList = Utils.findRequiredViewAsType(source, R.id.rcvUserList, "field 'rcvUserList'", RecyclerView.class);
    target.tvNoDataFound = Utils.findRequiredViewAsType(source, R.id.tvNoDataFound, "field 'tvNoDataFound'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SearchCandidateActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.etUserSearch = null;
    target.rcvUserList = null;
    target.tvNoDataFound = null;
  }
}
