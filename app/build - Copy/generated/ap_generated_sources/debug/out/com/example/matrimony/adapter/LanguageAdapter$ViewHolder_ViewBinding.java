// Generated code from Butter Knife. Do not modify!
package com.example.matrimony.adapter;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.example.matrimony.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LanguageAdapter$ViewHolder_ViewBinding implements Unbinder {
  private LanguageAdapter.ViewHolder target;

  @UiThread
  public LanguageAdapter$ViewHolder_ViewBinding(LanguageAdapter.ViewHolder target, View source) {
    this.target = target;

    target.tvName = Utils.findRequiredViewAsType(source, R.id.tvName, "field 'tvName'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    LanguageAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvName = null;
  }
}
